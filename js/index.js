$('document').ready(function() {
	var options = { videoId: 'TOocdZNOGDc', mute: false };
	$('#wrapper').tubular(options);
    $('.ac').position({
        of: $(document),
        my: 'center center',
        at: 'center center',
        collision: 'flip flip'
    });
    var parsley = $('#contactform').parsley();
    $('input,textarea').on("keyup change",function() {
        if(parsley.isValid()) {
            $('#ifvalid').slideDown('slow');
            parsley.validate();
        } else
            $('#ifvalid').slideUp('slow');
    });
//    $('#contactus').click(function() {
//        $('#contact').slideToggle('slow');
//        $("html, body").animate({ scrollTop: $("#contact").scrollTop() }, 1000);
//
//    });
    //console.log("done");
});