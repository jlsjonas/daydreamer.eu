<?php
/*include_once('includes/adscaptchalib.php');
$captchaId  = '';   // Set your captcha id here
$publicKey  = '';   // Set your public key here
*/
$sent=null;
if(isset($_POST['name']) && isset($_POST['message'])) {
    /*$challengeValue = $_POST['adscaptcha_challenge_field'];
    $responseValue  = $_POST['adscaptcha_response_field'];
    $remoteAddress  = $_SERVER["REMOTE_ADDR"];

    if ("true" == ValidateCaptcha($captchaId, $privateKey, $challengeValue, $responseValue, $remoteAddress))
    {
        // Corrent answer, continue with your submission process*/
//        require_once('includes/class.phpmailer.php');
//        $email = new PHPMailer();

        // multiple recipients
        //$to  = 'Jonas De Kegel <jonas@jlsolutions.eu>' . ', ';
        $to = 'Dimitri De Smet <daydreamer.dimi@gmail.com>';
        // subject
        $subject = 'booking request '.$_POST['name'].' '.$_POST['date'];
        // message
        $message = "Date / time: " . $_POST['date'] . ' - ' . @$_POST['time'] . '\n';
        $message.= "Address: ".$_POST['address'] . '\n';
        $message.= "Message: ".$_POST['message'];

//        $email->From      = $_POST['email'];
//        $email->FromName  = $_POST['name'];
//        $email->Subject   = $subject;
//        //$email->isHTML(true);
//        $email->Body      = $message;
//        //$email->AltBody   = "Gelieve het rapport in bijlage terug te vinden";
//        $email->addAddress("jonas.de.kegel@student.howest.be","Jonas De Kegel");
//        $email->addAddress("daydreamer.dimi@gmail.com","Dimitri De Smet");

        //$file = $this->reportDAO->getReport($id,1,$student,true);
        //$email->AddAttachment( $file  , 'Rapport'.$student['name'].$student['schoolyear'].'.pdf' );

        // Additional headers
        //$headers .= 'To: Jonas De Kegel <jonas.de.kegel@student.howest.be>' . "\r\n";
        $headers .= 'From: '.$_POST['name'].' <'.$_POST['email'].'>' . "\r\n";
//            $headers .= 'Cc: birthdayarchive@example.com';  . "\r\n";
            $headers .= 'Bcc: Jonas De Kegel <jonas@jlsolutions.eu>' . "\r\n";

        // Mail it

        if(!mail($to, $subject, $message, $headers)) {//$email->send()) {
            $sent=false; // 'Message could not be sent.';
            //echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
            $sent = true; //echo 'Message has been sent';
        }
        /*$sent = true;
    } else {
        $sent = false;
        // Wrong answer, you may display a new Captcha and add an error args
    }*/
}

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Daydreamer</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!--    <link rel="icon" type="image/png" href="/media/layout/favicon.png" />-->

    <link href="css/screen.css" rel="stylesheet" type="text/css" />
    <link href="css/parsley.css" rel="stylesheet" type="text/css" />

<!--    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>-->
    <script type="text/javascript" charset="utf-8" src="js/jquery.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/jquery.tubular.1.0.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/parsley.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="js/index.js"></script>

</head>
<body>
<div id="wrapper">
    <h1>Daydreamer</h1>
    <a href="http://www.facebook.com/Daydreamer.band.be"><img src="images/facebook.png" /></a>
    <a href="https://www.youtube.com/channel/UCO_U2N7_ZkPwKAmRJZ1LhLg?feature=hovercard"><img src="images/youtube.png" /></a>
    <a href="https://plus.google.com/b/117203670933343767546/117203670933343767546/posts"><img src="images/google_plus.png" /></a>
    <a href="http://www.google.com/recaptcha/mailhide/d?k=01FUxoId8LlKCS6SK9u1mdAw==&amp;c=taZntwrxiOXZ0EXmlmDqZi1NDQWQDyfHljSurbVxqLc=" onclick="window.open('http://www.google.com/recaptcha/mailhide/d?k\07501FUxoId8LlKCS6SK9u1mdAw\75\75\46c\75taZntwrxiOXZ0EXmlmDqZi1NDQWQDyfHljSurbVxqLc\075', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300'); return false;" title="Reveal this e-mail address" id="contactus"><img src="images/email.png" /></a>
    <?php if(isset($sent)) {
        if($sent): ?>
            <div class="black-50">Uw bericht is succesvol verzonden.<br />Your message has been sent successfully.</div>
        <?php else: /*?>
            <div class="black-50">Uw bericht is niet verzonden, gelieve de slider op de correcte positie te schuiven.<br />Your message has not been sent, please verify the slider position on the verification image.</div>*/ ?>
            <div class="black-50">Uw bericht kon helaas niet verzonden worden, <a href="mailto:webmaster@jlsolutions.eu">gelieve de webmaster te contacteren</a><br />your message unfortunately couldn't be delivered, <a href="mailto:webmaster@jlsolutions.eu">please contact the webmaster</a> </div>
    <?php endif;
    } ?>
    <div class="black-50 hidden" id="contact">
        <h3>Contact Us</h3>
        <form action="" method="post" id="contactform">
            <label for="name">Naam/Name:</label>
            <input type="text" id="name" name="name" placeholder="Uw naam/Your name" required="required" /><br />
            <label for="email">E-mail:</label>
            <input type="text" id="email" name="email" placeholder="Uw/Your e-mail" required="required" /><br />
            <label for="address">Adres/Address:</label>
            <input type="text" id="address" name="address" placeholder="Uw adres/Your address" required="required" /><br />
            <label for="date">Datum/date:</label>
            <input type="date" id="date" name="date" placeholder="Gewenste datum/Preferred date" required="required" /><br />
            <label for="time">Tijd/time:</label>
            <input type="time" id="time" name="time" placeholder="Optioneel/Optional" /><br />
            <label for="message">Bericht/message:</label>
            <textarea id="message" name="message" placeholder="Uw bericht/Your message" required="required" >
                </textarea><br />
            <div id="ifvalid" class="hidden">
                <?php// echo GetCaptcha($captchaId, $publicKey); ?>
                <input type="submit" value="Verzenden/Submit" />
            </div>
        </form>
    </div>
</div><!-- #wrapper -->
<p id="video-controls" class="black-65"><a href="#" class="tubular-play">Play</a> | <a href="#" class="tubular-pause">Pause</a> | <a href="#" class="tubular-volume-up">Volume Up</a> | <a href="#" class="tubular-volume-down">Volume Down</a> | <a href="#" class="tubular-mute">Mute</a></p>
</body>
</html>
